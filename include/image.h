#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

// Определение структуры пикселя.
struct __attribute__((packed)) pixel {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};

// Определение структуры изображения.
struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* pixels;
};

// Функции для работы с изображениями.
struct image create_image(uint64_t width, uint64_t height);
void destroy_image(struct image* img);
struct image duplicate_image(const struct image* source);
struct pixel get_pixel_at(const struct image* img, uint64_t x, uint64_t y);
void set_pixel_at(struct image* img, uint64_t x, uint64_t y, struct pixel px);
size_t calculate_line_size(const struct image* img);
#endif
