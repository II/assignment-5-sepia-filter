#include "bmp_format.h"
#include "image.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Функция для чтения BMP заголовка
static struct bmp_header read_bmp_header(FILE* file) {
    struct bmp_header header;
    fread(&header, sizeof(header), 1, file);
    return header;
}

// Функция для чтения изображения из BMP файла
enum read_status from_bmp(FILE* file, struct image* img) {
    struct bmp_header header = read_bmp_header(file);

    // Проверки на валидность BMP файла
    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE; // Неверная сигнатура
    }
    if (header.biBitCount != BITS_PER_PIXEL) {
        return READ_INVALID_BITS; // Неподдерживаемый формат пикселей
    }

    fseek(file, header.bOffBits, SEEK_SET);

    *img = create_image(header.biWidth, header.biHeight);
    if (!img->pixels) {
        return READ_INVALID_HEADER; // Ошибка чтения заголовка или выделения памяти
    }

    size_t row_size = header.biWidth * BYTES_PER_PIXEL;
    size_t row_padded = row_size + (BMP_PADDING - row_size % BMP_PADDING) % BMP_PADDING;
    unsigned char* row = malloc(row_padded);
    if (!row) {
        return READ_ERROR; // Ошибка выделения памяти
    }

    for (uint32_t y = 0; y < header.biHeight; y++) {
        fread(row, row_padded, 1, file);
        for (uint32_t x = 0; x < header.biWidth; x++) {
            size_t i = x * BYTES_PER_PIXEL;
            img->pixels[y * header.biWidth + x] = (struct pixel){row[i], row[i + 1], row[i + 2]};
        }
    }

    free(row);
    return READ_OK;
}



// Функция для записи изображения в BMP файл
enum write_status to_bmp(FILE* file, const struct image* img) {
    const uint32_t padding = (BMP_PADDING - (img->width * BYTES_PER_PIXEL) % BMP_PADDING) % BMP_PADDING;
    const uint32_t scanline_size = img->width * BYTES_PER_PIXEL + padding;
    const uint32_t file_size = sizeof(struct bmp_header) + scanline_size * img->height;

    struct bmp_header header = {
        .bfType = BMP_SIGNATURE,  // "BM"
        .bfileSize = file_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BITS_PER_PIXEL,  // Бит на пиксель
        .biCompression = NO_COMPRESSION,
        .biSizeImage = scanline_size * img->height,
        .biXPelsPerMeter = DEFAULT_PELS_PER_METER,
        .biYPelsPerMeter = DEFAULT_PELS_PER_METER,
        .biClrUsed = NO_CLR_USED,
        .biClrImportant = 0
    };

    if (fwrite(&header, sizeof(header), 1, file) != 1) {
        return WRITE_ERROR;
    }

    unsigned char* row = malloc(scanline_size);
    if (!row) {
        return WRITE_ERROR;
    }

    // Заполнение буфера нулями
    for (uint32_t i = 0; i < scanline_size; ++i) {
        row[i] = 0;
    }

    for (uint32_t y = 0; y < img->height; y++) {
        for (uint32_t x = 0; x < img->width; x++) {
            struct pixel px = img->pixels[y * img->width + x];
            size_t i = x * BYTES_PER_PIXEL;
            row[i] = px.blue;
            row[i + 1] = px.green;
            row[i + 2] = px.red;
        }
        if (fwrite(row, 1, scanline_size, file) != scanline_size) {
            free(row);
            return WRITE_ERROR;
        }
    }
    free(row);
    return WRITE_OK;
}
