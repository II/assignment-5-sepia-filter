#include "image.h"

#include <stdint.h>
#include <stdlib.h>

// Создание нового изображения с заданными размерами
struct image create_image(uint64_t width, uint64_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.pixels = malloc(width * height * sizeof(struct pixel));
    return img;
}

// Освобождение памяти, выделенной под изображение
void destroy_image(struct image* img) {
    // Проверка, что img не равен NULL
    if (img == NULL) {
        return;
    }

    // Освобождение памяти, выделенной под пиксели, если она не равна NULL
    if (img->pixels != NULL) {
        free(img->pixels);
        img->pixels = NULL;
    }

    // Обнуление полей структуры
    img->width = 0;
    img->height = 0;
}


// Создание копии изображения
struct image duplicate_image(const struct image* source) {
    struct image copy = create_image(source->width, source->height);
    if (copy.pixels) {
        for (uint64_t i = 0; i < source->width * source->height; ++i) {
            copy.pixels[i] = source->pixels[i];
        }
    }
    return copy;
}

// Получение пикселя по заданным координатам
struct pixel get_pixel_at(const struct image* img, uint64_t x, uint64_t y) {
    if (x < img->width && y < img->height) {
        return img->pixels[y * img->width + x];
    }
    return (struct pixel){0, 0, 0};  // Возвращаем черный пиксель в случае ошибки
}

// Установка пикселя по заданным координатам
void set_pixel_at(struct image* img, uint64_t x, uint64_t y, struct pixel px) {
    if (x < img->width && y < img->height) {
        img->pixels[y * img->width + x] = px;
    }
}

// Расчет размера строки в пикселях
size_t calculate_line_size(const struct image* img) {
    return img->width * sizeof(struct pixel);
}
