#include "sepia.h"
#include "image.h"

static inline double my_fmin(double a, double b) {
    return a < b ? a : b;
}

struct image sepia(const struct image* img) {
    struct image sepia_img = create_image(img->width, img->height);
    if (!sepia_img.pixels) return sepia_img;

    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            struct pixel old_px = get_pixel_at(img, x, y);
            struct pixel new_px;

            new_px.red = my_fmin(255, 0.393 * old_px.red + 0.769 * old_px.green + 0.189 * old_px.blue);
            new_px.green = my_fmin(255, 0.349 * old_px.red + 0.686 * old_px.green + 0.168 * old_px.blue);
            new_px.blue = my_fmin(255, 0.272 * old_px.red + 0.534 * old_px.green + 0.131 * old_px.blue);

            set_pixel_at(&sepia_img, x, y, new_px);
        }
    }

    return sepia_img;
}
