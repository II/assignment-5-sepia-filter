#include "bmp_format.h"
#include "file_utils.h"
#include "sepia.h"
#include "asm_sepia.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define REQUIRED_ARG_COUNT 4

int main(int argc, char* argv[]) {
    if (argc != REQUIRED_ARG_COUNT) {
        fprintf(stderr, "Использование: %s <входное изображение> <выходное изображение ASM> <выходное изображение C>\n", argv[0]);
        return 1;
    }

    const char* input_filename = argv[1];
    const char* output_filename_asm = argv[2];
    const char* output_filename_c = argv[3];

    FILE* input_file = open_file_read(input_filename);
    if (input_file == NULL) {
        perror("Ошибка при открытии входного файла");
        return 1;
    }

    struct image img;
    enum read_status read_status = from_bmp(input_file, &img);
    fclose(input_file);
    if (read_status != READ_OK) {
        fprintf(stderr, "Ошибка чтения BMP файла: %d\n", read_status);
        return 1;
    }

    // Измерение времени для версии на C
    clock_t start_c = clock();
    struct image sepia_img_c = sepia(&img);
    clock_t end_c = clock();
    double time_spent_c = (double)(end_c - start_c) / CLOCKS_PER_SEC;

    // Измерение времени для ассемблерной версии
    clock_t start_asm = clock();
    struct image sepia_img_asm = sepia_asm(&img);
    clock_t end_asm = clock();
    double time_spent_asm = (double)(end_asm - start_asm) / CLOCKS_PER_SEC;

    // Запись результата ассемблерной версии
    FILE* output_file_asm = open_file_write(output_filename_asm);
    if (output_file_asm == NULL) {
        perror("Ошибка при открытии выходного файла для ASM");
        destroy_image(&sepia_img_asm);
        return 1;
    }
    enum write_status write_status_asm = to_bmp(output_file_asm, &sepia_img_asm);
    fclose(output_file_asm);
    destroy_image(&sepia_img_asm);

    // Запись результата версии на C
    FILE* output_file_c = open_file_write(output_filename_c);
    if (output_file_c == NULL) {
        perror("Ошибка при открытии выходного файла для C");
        destroy_image(&sepia_img_c);
        return 1;
    }
    enum write_status write_status_c = to_bmp(output_file_c, &sepia_img_c);
    fclose(output_file_c);
    destroy_image(&sepia_img_c);

    // Проверка результатов записи
    if (write_status_asm != WRITE_OK || write_status_c != WRITE_OK) {
        fprintf(stderr, "Ошибка записи BMP файла\n");
        return 1;
    }

    printf("Изображение успешно обработано и сохранено в %s (ASM) и %s (C)\n", output_filename_asm, output_filename_c);
    printf("Время выполнения ASM версии: %f секунд\n", time_spent_asm);
    printf("Время выполнения C версии: %f секунд\n", time_spent_c);

    return 0;
}
