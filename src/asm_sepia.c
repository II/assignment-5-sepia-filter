#include "image.h"

extern void apply_sepia_pixel(struct pixel* px);

struct image sepia_asm(const struct image* img) {
    struct image new_img = create_image(img->width, img->height);
    if (!new_img.pixels) return new_img;

    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            struct pixel* original_px = &img->pixels[y * img->width + x];
            apply_sepia_pixel(original_px);
            set_pixel_at(&new_img, x, y, *original_px);
        }
    }

    return new_img;
}
