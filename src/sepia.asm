%macro convert_byte_to_float 2
    movzx eax, byte [%1]       ; Загрузка байта и расширение до 32 бит с обнулением старших битов
    cvtsi2ss %2, eax           ; Преобразование из целого в число с плавающей точкой и загрузка в xmm регистр
    shufps %2, %2, 0           ; Дублирование значения по всему xmm регистру
%endmacro

section .rodata

blue_coeffs: dd 0.131, 0.168, 0.189, 0
green_coeffs: dd 0.534, 0.686, 0.769, 0
red_coeffs: dd 0.272, 0.349, 0.393, 0

section .text
global apply_sepia_pixel

apply_sepia_pixel:
    mov r8, [rdi + 3]

    convert_byte_to_float rdi, xmm0
    convert_byte_to_float rdi + 1, xmm1
    convert_byte_to_float rdi + 2, xmm2

    movaps xmm3, [blue_coeffs]
    movaps xmm4, [green_coeffs]
    movaps xmm5, [red_coeffs]

    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    movd [rdi], xmm0
    mov [rdi + 3], r8
    ret
